#!/bin/bash

EVENTBIKE_ENVIRONMENT=$1

if [ -f ~/.config/eventbike.conf ]; then
	source ~/.config/eventbike.conf
else
	echo "Eventbike config not found."
	exit 1
fi

: ${EVENTBIKE_ENVIRONMENT:=staging}
: ${EVENTBIKE_SECRET_PATH:=../syncthing/secrets}

~/.local/bin/mkdocs build
rsync -aP --delete --progress site/* -e "ssh -i '${EVENTBIKE_SECRET_PATH}/id_eventbike'" . "team@eventbikezero.de:eventbikezero.de/$EVENTBIKE_ENVIRONMENT/"
