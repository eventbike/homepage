# eventbike homepage

This is the generator setup for the eventbike_zero website. The content itself is hosted in our private cloud (to allow all team members to edit it + save Codeberg from hosting our images). The content folder is symlinked to the `docs` folder.

## Setup

On Debian:
~~~
sudo apt install python3-pip
pip3 install -r requirements.txt
~~~

Then link the actual homepage content from the cloud to the `docs/` folder, like so:

~~~
ln -s ~/Dokumente/path-to-the-eventbike-cloud/homepage-content docs
~~~

You can edit the file `.settings` to make sure the deploy script finds your `secrets` folder with the deploy key.


## Updating the site

Edit the files in the docs/ folder or in the cloud with your favourite text editor.
We use Markdown for the pages, [read more on the mkdocs website](https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown).

To preview your changes locally, run

~~~
mkdocs serve
~~~

and open http://127.0.0.1:8000/ in your web browser.

If you are done with your changes, just run `deploy.sh`,
and optionally specify which environment to use (`web` goes live, or `staging` creates another preview)

~~~
./deploy.sh staging
~~~



## Licence

Files in this repository are licenced under a zero-clause BSD if not stated otherwise. You can find the licence text below:.

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
